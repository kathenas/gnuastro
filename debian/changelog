gnuastro (0.23-2) unstable; urgency=medium

  * Move to unstable after sufficient testing period.

 -- Phil Wyett <philip.wyett@kathenas.org>  Sat, 14 Sep 2024 17:41:18 +0100

gnuastro (0.23-1) experimental; urgency=medium

  * New upstream version 0.23.
  * Shared library version bump to 21.
  * Remove t64 transition.
  * Update 'Standards-Version' to 4.7.0, no changes needed.
  * Add patch: 001_remove_bash_redirection.patch
  * Move 'libgnuastro_make.so' to 'libgnuastro-dev' package. (Closes: #1052763)

 -- Phil Wyett <philip.wyett@kathenas.org>  Fri, 26 Jul 2024 22:45:55 +0100

gnuastro (0.22-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062183

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 19:20:35 +0000

gnuastro (0.22-3) unstable; urgency=medium

  * 'd/control': Drop not required 'libtool-bin' Build-Depends.
  * 'd/control': Add 'libtool' to Build-Depends.
  * 'd/control': Use 'debhelper-compat' 13.
  * Install previously not installed files:
    - '/usr/share/gnuastro/pointing-simulate.mk'
    - '/usr/share/gnuastro/zeropoint.mk'
  * 'd/rules': Skip 'dwz' as compression is not benefitial.

 -- Phil Wyett <philip.wyett@kathenas.org>  Tue, 20 Feb 2024 08:43:34 +0000

gnuastro (0.22-2) unstable; urgency=medium

  * 'd/rules':
    - Remove unused '.la' and '.a' files here rather than 'd/not-installed'.
    - Remove left over programs built and fix 'build twice'. (Closes: #1044300)

 -- Phil Wyett <philip.wyett@kathenas.org>  Sun, 11 Feb 2024 06:15:29 +0000

gnuastro (0.22-1) unstable; urgency=medium

  * 'd/control': Add uploader Phil Wyett <philip.wyett@kathenas.org>
  * 'd/control': Fix file conflicts with 'Replaces' and 'Breaks'.
    (Closes: #1052763)
  * 'd/control': Make long description more generic. No versioned names that
    maybe missed during an update of the package.

 -- Phil Wyett <philip.wyett@kathenas.org>  Sat, 03 Feb 2024 19:23:37 +0000

gnuastro (0.20-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sun, 13 Aug 2023 21:46:13 +0200


gnuastro (0.19.106-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 19 Apr 2023 20:06:12 +0200


gnuastro (0.19-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 24 Oct 2022 13:32:11 +0200


gnuastro (0.18.70-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sun, 09 Oct 2022 11:44:10 +0200


gnuastro (0.18-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Thu, 21 Jul 2022 20:38:09 +0100


gnuastro (0.17.64-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Fri, 24 Jun 2022 19:07:08 +0200


gnuastro (0.17-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sun, 20 Mar 2022 03:55:07 +0100

gnuastro (0.16.187-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Fri, 18 Mar 2022 00:02:06 +0100

gnuastro (0.16.67-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sat, 08 Jan 2022 20:37:05 +0100

gnuastro (0.16.1-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 15 Nov 2021 14:54:02 +0100

gnuastro (0.15.54-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

  * Added copyright statement for bootstrapped/* files.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sun, 31 Oct 2021 00:02:01 +0200

gnuastro (0.15-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sat, 04 Sep 2021 16:16:01 +0100

gnuastro (0.14.98-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Thu, 15 Apr 2021 12:32:54 +0000

gnuastro (0.14-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 25 Jan 2021 13:14:02 +0000

gnuastro (0.13.93-1) experimental; urgency=low

  * New upstream version to fix new bug on Debian's Hurd system.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Tue, 12 Jan 2021 00:38:12 +0000

gnuastro (0.13.58-1) experimental; urgency=low

  * New upstream version to fix bug on Debian's Hurd system.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 25 Nov 2020 13:20:13 +0000

gnuastro (0.13.50-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Thu, 12 Nov 2020 18:53:21 +0000

gnuastro (0.13-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 07 Sep 2020 02:21:41 +0200

gnuastro (0.12.37-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sun, 09 Aug 2020 04:02:01 +0200

gnuastro (0.12-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 20 May 2020 17:36:19 +0100

gnuastro (0.11.52-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 20 Apr 2020 04:20:28 +0100

gnuastro (0.11-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 25 Nov 2019 18:00:01 +0000

gnuastro (0.10.39-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Fri, 18 Oct 2019 16:23:42 +0000

gnuastro (0.10-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sat, 03 Aug 2019 02:53:11 +0100

gnuastro (0.9.54-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Tue, 11 Jun 2019 15:59:10 +0100

gnuastro (0.9-2) experimental; urgency=low

  * Switch to experimental to allow build during freeze of Buster.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 22 Apr 2019 13:49:47 +0100

gnuastro (0.9-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 17 Apr 2019 18:59:02 +0200

gnuastro (0.8.75-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 10 Apr 2019 00:05:15 +0200

gnuastro (0.8.36-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 20 Feb 2019 17:49:15 +0000

gnuastro (0.8-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Fri, 28 Dec 2018 12:52:28 +0200

gnuastro (0.7.68-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sat, 08 Dec 2018 16:46:21 +0200

gnuastro (0.7.48-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Thu, 11 Oct 2018 16:41:02 +0200

gnuastro (0.7.42-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sat, 22 Sep 2018 10:50:27 +0200

gnuastro (0.7-1) unstable; urgency=low

  * New upstream version.

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 08 Aug 2018 20:56:41 +0200

gnuastro (0.6.47-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Sat, 21 Jul 2018 18:06:02 +0200

gnuastro (0.6.8-1) unstable; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Wed, 06 Jun 2018 00:29:42 +0200

gnuastro (0.6-1) unstable; urgency=low

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Mon, 04 Jun 2018 18:59:27 +0200

gnuastro (0.5.127-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Fri, 18 May 2018 16:28:02 +0200

gnuastro (0.5.75-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <mohammad@akhlaghi.org>  Fri, 13 Apr 2018 02:20:16 +0200

gnuastro (0.5-1) unstable; urgency=low

  * Switch to unstable for upstream stable release.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Wed, 17 Jan 2018 17:24:05 +0200

gnuastro (0.4.121-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Thu, 14 Dec 2017 04:55:02 +0200

gnuastro (0.4.120-1) experimental; urgency=low

  * New upstream version.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Wed, 13 Dec 2017 18:19:02 +0200

gnuastro (0.4.53-1) experimental; urgency=low

  * New upstream version.

  * Switch to experimental to prepare transition.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Tue, 7 Nov 2017 15:20:16 +0200

gnuastro (0.4-1) unstable; urgency=low

  * This is a major Gnuastro release, so the Debian release is now also
    being changed to unstable.

  * All programs: `.fit' is now a recognized FITS file suffix.

  * All programs: ASCII text files (tables) created with CRLF line
    terminators (for example text files created in MS Windows) are now also
    readable as input when necessary.

  * Arithmetic: now has a new `--globalhdu' (`-g') option which can be used
    once for all the input images.

  * MakeNoise: with the new `--sigma' (`-s') option, it is now possible to
    directly request the noise sigma or standard deviation. When this option
    is called, the `--background', `--zeropoint' and other option values will
    be ignored.

  * MakeProfiles: the new `distance' profile will save the radial distance of
    each pixel. This may be used to define your own profiles that are not
    currently supported in MakeProfiles.

  * MakeProfiles: with the new `--mcolisbrightness' ("mcol-is-brightness")
    option, the `--mcol' values of the catalog will be interpretted as total
    brightness (sum of pixel values), not magnitude.

  * NoiseChisel: with the new `--dilatengb' option, it is now possible to
    identify the connectivity of the final dilation.

  * Library: Functions that read data from an ASCII text file
    (`gal_txt_table_info', `gal_txt_table_read', `gal_txt_image_read') now
    also operate on files with CRLF line terminators.

  * Crop: The new `--center' option is now used to define the center of a
    single crop. Hence the old `--ra', `--dec', `--xc', `--yc' have been
    removed. This new option can take multiple values (one value for each
    dimension). Fractions are also acceptable.

  * Crop: The new `--width' option is now used to define the width of a
    single crop. Hence the old `--iwidth', `--wwidth' were removed. The units
    to interpret the value to the option are specified by the `--mode'
    option. With the new `--width' option it is also possible to define a
    non-square crop (different widths along each dimension). In WCS mode, its
    units are no longer arcseconds but are the same units of the WCS (degrees
    for angles). `--width' can also accept fractions. So to set a width of 5
    arcseconds, you can give it a value of `5/3600' for the angular
    dimensions.

  * Crop: The new `--coordcol' option is now used to determine the catalog
    columns that define coordinates. Hence the old `--racol', `--deccol',
    `--xcol', and `--ycol' have been removed. This new option can be called
    multiple times and the order of its calling will be used for the column
    containing the center in the respective dimension (in FITS format).

  * MakeNoise: the old `--stdadd' (`-s') option has been renamed to
    `--instrumental' (`-i') to be more clear.

  * `gal_data_free_contents': when the input `gal_data_t' is a tile, its
    `array' element will not be freed. This enables safe usage of this
    function (and thus `gal_data_free') on tiles without worrying about the
    memory block associated with the tile.

  * `gal_box_bound_ellipse' is the new name for the old
    `gal_box_ellipse_in_box' (to be more clear and avoid repetition of the
    term `box'). The input position angle is now also in degrees, not
    radians.

  * `gal_box_overlap' now works on data of any dimensionality and thus also
    needs the number of dimensions (elements in each input array).

  * `gal_box_border_from_center' now accepts an array of coordinates as one
    argument and the number of dimensions as another. This allows it to work
    on any dimensionality.

  * `gal_fits_img_info' now also returns the name and units of the dataset
    (if they aren't NULL). So it takes two extra arguments.

  * `gal_wcs_pixel_scale' now replaces the old `gal_wcs_pixel_scale_deg',
    since it doesn't only apply to degrees. The pixel scale units are defined
    by the units of the WCS.

  * `GAL_TILE_PARSE_OPERATE' (only when `OTHER' is given) can now parse and
    operate on different datasets independent of the size of allocated block
    of memory (the tile sizes of `IN' and `OTHER' have to be identical, but
    not their allocated blocks of memory). Until now, it was necessary for
    the two blocks to have the same size and this is no longer the case.

  * Warp's align matrix when second dimension must be reversed - fixed.

  * Reading BZERO for unsigned 64-bit integers - fixed.

  * Arithmetic with one file and no operators - fixed.

  * NoiseChisel segfault when detection contains no clumps - fixed.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Wed, 13 Sep 2017 01:58:54 +0200

gnuastro (0.3.55-1) experimental; urgency=low

  * A test release to check correct compilation on all architectures
    ahead of the 0.4 release.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Fri, 25 Aug 2017 14:35:21 +0200

gnuastro (0.3.33-2) unstable; urgency=low

  * Explicit version of libgit removed from dependencies (Closes: #872857)

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Tue, 22 Aug 2017 12:51:12 +0200

gnuastro (0.3.33-1) unstable; urgency=low

  * Crashes on 32-bit and big-endian systems corrected. (Closes: #868586)
    (Closes: #868587)

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Sun, 16 Jul 2017 00:08:12 +0200

gnuastro (0.3.31-1) unstable; urgency=low

  * MakeProfiles: the new `--pc', `--cunit' and `--ctype' options can
    be used to specify the PC matrix, CUNIT and CTYPE world coordinate
    system keywords of the output FITS file.

  * MakeProfiles: The new `--naxis' and `--shift' options can take multiple
    values for each dimension (separated by a comma). This replaces the old
    `--naxis1', `--naxis2' and `--xshift' and `--yshift' options.

  * MakeProfiles: The new `--ccol' option can take the center coordinate
    columns of the catalog (in multiple calls) and the new `--mode' option is
    used to identify what standard to interpret them in (image or
    WCS). Together, these replace the old `--xcol', `--ycol', `--racol' and
    `--deccol'.

  * MakeProfiles: The new `--crpix', `--crval' and `--cdelt' options now
    accept multiple values separated by a comma. So they replace the old
    `--crpix1', `--crpix2', `--crval1', `--crval2' and `--resolution'
    options.

  * Bug fixed: Pure rotation around pixel coordinate (0,0).

  * Bug fixed: NoiseChisel segfault when no usable region for sky clumps.

  * Bug fixed: Pixel scale measurement when dimension scale isn't equal
    or doesn't decrease.

  * Bug fixed: Improper types for function code in MakeProfiles.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Fri, 14 Jul 2017 20:11:52 +0200


gnuastro (0.3.23-1) unstable; urgency=low

  * Fixes issue with 32-bit big-endian systems (Closes: #865973)

  * Added features: MakeProfiles can now build a kernel profile
    without a catalog. Defining a crop by its center is now unified
    through the `--center' option, same for `--width'.

  * Library soname set to 2 in preparation for version 0.4.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Thu, 29 Jun 2017 18:16:02 +0200


gnuastro (0.3.13-1) unstable; urgency=low

  * Second update. (Closes: #865780)

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Sun, 25 Jun 2017 18:45:32 +0200

gnuastro (0.3.7-1) unstable; urgency=low

  * First update.

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Mon, 12 Jun 2017 14:38:07 +0200

gnuastro (0.2.33-1) unstable; urgency=low

  * Initial release. (Closes: #841365)

 -- Mohammad Akhlaghi <akhlaghi@gnu.org>  Thu, 27 Oct 2016 23:49:18 +0200
